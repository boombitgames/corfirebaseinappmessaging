#import "Firebase.h"

@interface FirebaseInAppMessaging : NSObject<FIRInAppMessagingDisplayDelegate>

+ (FirebaseInAppMessaging*)sharedManager;

- (void)initialize;

- (void)triggerEventWithName:(NSString*)eventName;

- (void)supressDisplay:(bool)supressDisplay;

- (void)setAutomaticDataCollection:(bool)autoCollectionEnabled;

@end
