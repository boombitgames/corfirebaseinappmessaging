#import "FirebaseInAppMessaging.h"
#import <CORCommon/CORCommon.h>

@implementation FirebaseInAppMessaging

UIView *UnityGetGLView();

+ (FirebaseInAppMessaging*)sharedManager
{
    static FirebaseInAppMessaging *sharedSingleton;
    
    if( !sharedSingleton )
    {
        sharedSingleton = [[FirebaseInAppMessaging alloc] init];
    }
    
    return sharedSingleton;
}

- (void)initialize
{
    [FIRInAppMessaging inAppMessaging].delegate=self;
}

- (void)triggerEventWithName:(NSString*)eventName
{
    NSLog(@"FirebaseInAppMessaging :: triggerEventWithName :: eventName: %@", eventName);
    [[FIRInAppMessaging inAppMessaging] triggerEvent:eventName];
}

- (void)supressDisplay:(bool)supressDisplay
{
    [[FIRInAppMessaging inAppMessaging] setMessageDisplaySuppressed:supressDisplay];
}

- (void)setAutomaticDataCollection:(bool)autoCollectionEnabled
{
    [[FIRInAppMessaging inAppMessaging] setAutomaticDataCollectionEnabled:autoCollectionEnabled];
}

#pragma mark - FIRInAppMessagingDisplayDelegate

- (void)displayErrorForMessage:(nonnull FIRInAppMessagingDisplayMessage *)inAppMessage
                         error:(nonnull NSError *)error
{
    
}

- (void)impressionDetectedForMessage:(nonnull FIRInAppMessagingDisplayMessage *)inAppMessage
{
    NSString* jsonString=@"{}";
    
    if (inAppMessage.appData)
    {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inAppMessage.appData
                                                           options:0
                                                             error:&error];
        if (! jsonData) {
            NSLog(@"Got an error: %@", error);
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
    }
    
    UnitySendMessage("FirebaseInAppMessaging", "OnMessageReceived", jsonString.UTF8String);
}

- (void)messageClicked:(FIRInAppMessagingDisplayMessage *)inAppMessage withAction:(FIRInAppMessagingAction *)action
{
    if (action.actionText)
    {
        UnitySendMessage("FirebaseInAppMessaging", "OnMessageClicked", action.actionText.UTF8String);
    }
}

- (void)messageDismissed:(nonnull FIRInAppMessagingDisplayMessage *)inAppMessage dismissType:(FIRInAppMessagingDismissType)dismissType
{
    
}

@end

extern "C" {
void FIRInAppMsgInitialize()
{
    [[FirebaseInAppMessaging sharedManager] initialize];
}

void FIRInAppMsgTriggerEvent(char* eventName)
{
    [[FirebaseInAppMessaging sharedManager] triggerEventWithName:GetStringParam(eventName)];
}

void FIRInAppMsgSupressDisplay(bool supressDisplay)
{
    [[FirebaseInAppMessaging sharedManager] supressDisplay:supressDisplay];
}

void FIRInAppMsgSetAutomaticDataCollection(bool autoCollectionEnabled)
{
    [[FirebaseInAppMessaging sharedManager] setAutomaticDataCollection:autoCollectionEnabled];
}

}
